package etf.santorini.sl150191d.structures;

import java.util.ArrayList;
import java.util.List;

public class Coordinate {
    private int row;
    private int column;

    public Coordinate(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * Staticka funkcija koja roverava da li su dve koordinate jednake
     */
    public static boolean sameCoordinate(Coordinate a, Coordinate b) {
        return a.getRow() == b.getRow() && a.getColumn() == b.getColumn();
    }

    /**
     * Vraca listu okolnih koordinata koje su unutar polja za igru
     */
    public List<Coordinate> surroundingCoordinates() {
        ArrayList<Coordinate> result = new ArrayList<>();

        for (int currRow = getRow() - 1; currRow < getRow() + 2; ++currRow)
            for (int currColumn = getColumn() - 1; currColumn < getColumn() + 2; ++currColumn) {
                Coordinate currCoordinate = new Coordinate(currRow, currColumn);

                if (Coordinate.sameCoordinate(currCoordinate, this) || !currCoordinate.inFieldRange())
                    continue;

                result.add(currCoordinate);
            }

        return result;
    }

    public int manhattanDistance(Coordinate coordinate) {

        return Math.abs(this.row - coordinate.row) + Math.abs(this.column - coordinate.column);
    }

    public int moveDistance(Coordinate coordinate) {
        int rowDist = Math.abs(this.row - coordinate.row);
        int columnDist = Math.abs(this.column - coordinate.column);

        return (rowDist > columnDist) ? rowDist : columnDist;
    }

    public Coordinate(String value) {
        if (value.length() != 2) {
            System.err.println("Error parsing string to coordinate: '" + value + "'");
            System.exit(1);
        }

        this.row = Character.getNumericValue(value.charAt(0)) - Character.getNumericValue('A');
        this.column = Character.getNumericValue(value.charAt(1)) - 1;
    }

    @Override
    public String toString() {
        String value = "";

        switch (this.row) {
            case (0):
                value = "A";
                break;
            case (1):
                value = "B";
                break;
            case (2):
                value = "C";
                break;
            case (3):
                value = "D";
                break;
            case (4):
                value = "E";
                break;
        }
        value += (this.column + 1);

        return value;
    }

    private boolean inFieldRange() {
        return row >= 0 && row < 5 && column >= 0 && column < 5;
    }

    /**
     * Provera da li su dve koordiante jedna pored druge.
     * Ako su iste koordinate vraca se false.
     */
    public boolean isNextTo(Coordinate coordinate) {
        if (Coordinate.sameCoordinate(this, coordinate))
            return false;

        return (Math.abs(this.row - coordinate.row) <= 1 && Math.abs(this.column - coordinate.column) <= 1);
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

}
