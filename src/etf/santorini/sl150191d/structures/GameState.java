package etf.santorini.sl150191d.structures;

import etf.santorini.sl150191d.globals.Globals;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GameState {
    // position of player max
    private Coordinate maxFigure1;
    private Coordinate maxFigure2;
    // position of player min
    private Coordinate minFigure1;
    private Coordinate minFigure2;

    private int[][] fieldLevels;

    private int playerOnTurn;

    public GameState(GameState gs) {
        super();
        this.maxFigure1 = gs.maxFigure1;
        this.maxFigure2 = gs.maxFigure2;
        this.minFigure1 = gs.minFigure1;
        this.minFigure2 = gs.minFigure2;
        this.playerOnTurn = gs.playerOnTurn;

        this.fieldLevels = new int[5][];
        for (int i = 0; i < 5; ++i) {
            this.fieldLevels[i] = gs.fieldLevels[i].clone();
        }
    }

    public GameState() {
        super();
        this.fieldLevels = new int[5][5];
    }

    /**
     * Kreirati GameState na osnovu sadrzaja fajla.
     */
    public GameState(String filePath) throws FileNotFoundException {
        super();
        this.fieldLevels = new int[5][5];

        Scanner scanner = new Scanner(new FileInputStream(filePath));


        if (scanner.hasNext()) {
            maxFigure1 = new Coordinate(scanner.next());
            maxFigure2 = new Coordinate(scanner.next());
        }
        if (scanner.hasNext()) {
            minFigure1 = new Coordinate(scanner.next());
            minFigure2 = new Coordinate(scanner.next());
        }

        playerOnTurn = Globals.PLAYER_MAX;

        while (scanner.hasNext()) {
            Coordinate selectedFigure = new Coordinate(scanner.next());
            Coordinate destination = new Coordinate(scanner.next());
            Coordinate buildField = new Coordinate(scanner.next());

            incrementFieldLevel(buildField);

            if (playerOnTurn == Globals.PLAYER_MAX) {
                if (Coordinate.sameCoordinate(maxFigure1, selectedFigure)) {
                    maxFigure1 = destination;   //max1
                } else if (Coordinate.sameCoordinate(maxFigure2, selectedFigure)) {
                    maxFigure2 = destination;   //max2
                } else {
                    System.err.println("Error in file. No figure on field: " + selectedFigure);
                    System.exit(1);
                }
            } else {
                if (Coordinate.sameCoordinate(minFigure1, selectedFigure)) {
                    minFigure1 = destination;   //min1
                } else if (Coordinate.sameCoordinate(minFigure2, selectedFigure)) {
                    minFigure2 = destination;   //min2
                } else {
                    System.err.println("Error in file. No figure on field: " + selectedFigure);
                    System.exit(1);
                }
            }
            playerOnTurn = -playerOnTurn;
        }
    }

    /**
     * Vraca listu svih sledecih mogucih poteza
     * Uzima u obzir da li je Min ili Max na potezu
     */
    public static ArrayList<GameState> possibleNextGameStates(GameState currGameState) {
        ArrayList<GameState> result = new ArrayList<>();

        switch (currGameState.playerOnTurn) {
            case Globals.PLAYER_MAX:
                result = availableMaxMoves(currGameState);
                break;

            case Globals.PLAYER_MIN:
                result = availableMinMoves(currGameState);
                break;
        }

        return result;
    }

    private static ArrayList<GameState> availableMaxMoves(GameState currGameState) {
        ArrayList<GameState> result = new ArrayList<>();

        //figure 1
        result.addAll(availableMax1Moves(currGameState));
        //figure 2
        result.addAll(availableMax2Moves(currGameState));

        return result;
    }

    private static ArrayList<GameState> availableMax1Moves(GameState currGameState) {
        ArrayList<GameState> result = new ArrayList<>();

        ArrayList<Coordinate> nextPositions = (ArrayList<Coordinate>) currGameState.getMaxFigure1().surroundingCoordinates();

        nextPositions.removeIf(currCord -> currGameState.isPositionUnreachable(currGameState.maxFigure1, currCord));

        for (Coordinate curr_cord : nextPositions) {
            GameState playerMoved = new GameState(currGameState);
            playerMoved.setMaxFigure1(curr_cord);

            for (Coordinate currBlockPoss : playerMoved.buildableAroundCoordinate(playerMoved.getMaxFigure1())) {
                GameState addedBlock = new GameState(playerMoved);
                addedBlock.setFieldLevel(currBlockPoss, addedBlock.getFieldLevel(currBlockPoss) + 1);
                addedBlock.setPlayerOnTurn(Globals.PLAYER_MIN);

                result.add(addedBlock);
            }

        }
        return result;
    }

    private static ArrayList<GameState> availableMax2Moves(GameState currGameState) {
        ArrayList<GameState> result = new ArrayList<>();

        ArrayList<Coordinate> nextPositions = (ArrayList<Coordinate>) currGameState.getMaxFigure2().surroundingCoordinates();

        nextPositions.removeIf(currCord -> currGameState.isPositionUnreachable(currGameState.maxFigure2, currCord));

        for (Coordinate currCord : nextPositions) {
            GameState playerMoved = new GameState(currGameState);
            playerMoved.setMaxFigure2(currCord);

            for (Coordinate currBlockPoss : playerMoved.buildableAroundCoordinate(currCord)) {
                GameState addedBlock = new GameState(playerMoved);

                addedBlock.setFieldLevel(currBlockPoss, addedBlock.getFieldLevel(currBlockPoss) + 1);
                addedBlock.setPlayerOnTurn(Globals.PLAYER_MIN);

                result.add(addedBlock);
            }

        }
        return result;
    }

    private static ArrayList<GameState> availableMinMoves(GameState currGameState) {
        ArrayList<GameState> result = new ArrayList<>();

        //figure 1
        result.addAll(availableMin1Moves(currGameState));
        //figure 2
        result.addAll(availableMin2Moves(currGameState));

        return result;
    }

    private static List<GameState> availableMin1Moves(GameState currGameState) {
        ArrayList<GameState> result = new ArrayList<>();

        ArrayList<Coordinate> nextPositions = (ArrayList<Coordinate>) currGameState.getMinFigure1().surroundingCoordinates();

        nextPositions.removeIf(currCord -> currGameState.isPositionUnreachable(currGameState.minFigure1, currCord));

        for (Coordinate currCord : nextPositions) {
            GameState playerMoved = new GameState(currGameState);
            playerMoved.setMinFigure1(currCord);

            for (Coordinate currBlockPoss : playerMoved.buildableAroundCoordinate(currCord)) {
                GameState addedBlock = new GameState(playerMoved);

                addedBlock.setFieldLevel(currBlockPoss, addedBlock.getFieldLevel(currBlockPoss) + 1);
                addedBlock.setPlayerOnTurn(Globals.PLAYER_MAX);

                result.add(addedBlock);
            }

        }
        return result;
    }

    private static List<GameState> availableMin2Moves(GameState currGameState) {
        ArrayList<GameState> result = new ArrayList<>();

        ArrayList<Coordinate> nextPositions = (ArrayList<Coordinate>) currGameState.getMinFigure2().surroundingCoordinates();

        nextPositions.removeIf(currCord -> currGameState.isPositionUnreachable(currGameState.minFigure2, currCord));

        for (Coordinate currCord : nextPositions) {
            GameState playerMoved = new GameState(currGameState);
            playerMoved.setMinFigure2(currCord);

            for (Coordinate currBlockPoss : playerMoved.buildableAroundCoordinate(currCord)) {
                GameState addedBlock = new GameState(playerMoved);

                addedBlock.setFieldLevel(currBlockPoss, addedBlock.getFieldLevel(currBlockPoss) + 1);
                addedBlock.setPlayerOnTurn(Globals.PLAYER_MAX);

                result.add(addedBlock);
            }

        }
        return result;
    }

    /**
     * Da li je pozicija dostizna od strane nekog igraca
     * Ako su koorditane iste, vraca se false
     */
    public boolean isPositionUnreachable(Coordinate startCoordinate, Coordinate destCoordinate) {
        if (Coordinate.sameCoordinate(startCoordinate, destCoordinate))
            return true;

        return (getFieldLevel(destCoordinate) > getFieldLevel(startCoordinate) + 1
                || isFieldOccupied(destCoordinate)
                || getFieldLevel(destCoordinate) == 4
                || !startCoordinate.isNextTo(destCoordinate));

    }

    /**
     * Sve koordinate na koje se moze smestiti blok
     */
    public ArrayList<Coordinate> buildableAroundCoordinate(Coordinate cord) {
        ArrayList<Coordinate> blockPositions = new ArrayList<>();

        for (Coordinate coordinate : cord.surroundingCoordinates()) {

            if (getFieldLevel(coordinate) > 3 || isFieldOccupied(coordinate))
                continue;

            blockPositions.add(coordinate);
        }

        return blockPositions;
    }

    /**
     * Da li je polje zauzeto od strane igraca
     */
    public boolean isFieldOccupied(Coordinate coordinate) {
        return (Coordinate.sameCoordinate(coordinate, maxFigure1) || Coordinate.sameCoordinate(coordinate, maxFigure2)
                || Coordinate.sameCoordinate(coordinate, minFigure1) || Coordinate.sameCoordinate(coordinate, minFigure2));
    }

    public int getFieldLevel(Coordinate c) {
        return fieldLevels[c.getRow()][c.getColumn()];
    }

    public void setFieldLevel(Coordinate c, int level) {
        fieldLevels[c.getRow()][c.getColumn()] = level;
    }

    public void incrementFieldLevel(Coordinate c) {
        ++fieldLevels[c.getRow()][c.getColumn()];
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder("GameState [ On turn: " + playerOnTurn + "]\n");

        for (int row = 0; row < 5; ++row) {
            for (int col = 0; col < 5; ++col) {
                if (Coordinate.sameCoordinate(maxFigure1, new Coordinate(row, col))) {
                    sb.append("X1[").append(fieldLevels[row][col]).append("]\t");
                } else if (Coordinate.sameCoordinate(maxFigure2, new Coordinate(row, col))) {
                    sb.append("X2[").append(fieldLevels[row][col]).append("]\t");
                } else if (Coordinate.sameCoordinate(minFigure1, new Coordinate(row, col))) {
                    sb.append("Y1[").append(fieldLevels[row][col]).append("]\t");
                } else if (Coordinate.sameCoordinate(minFigure2, new Coordinate(row, col))) {
                    sb.append("Y2[").append(fieldLevels[row][col]).append("]\t");
                } else {
                    sb.append(fieldLevels[row][col]).append("\t\t");
                }
            }

            sb.append("\n");
        }
        return sb.toString();

    }

    public boolean isMaxVictorious() {
        return getFieldLevel(maxFigure1) == 3
                || getFieldLevel(maxFigure2) == 3
                || (isBlocked(minFigure1) && isBlocked(minFigure2));
    }

    public boolean isMinVictorious() {
        return getFieldLevel(minFigure1) == 3
                || getFieldLevel(minFigure2) == 3
                || (isBlocked(maxFigure1) && isBlocked(maxFigure2));
    }

    /**
     * Proverava da li je neko na tom polju ili je nivo polja 4
     */
    public boolean isBuildable(Coordinate coordinate) {

        return (!isFieldOccupied(coordinate) && getFieldLevel(coordinate) != 4);
    }

    public Coordinate getMaxFigure1() {
        return maxFigure1;
    }

    public void setMaxFigure1(Coordinate maxFigure1) {
        this.maxFigure1 = maxFigure1;
    }

    public Coordinate getMaxFigure2() {
        return maxFigure2;
    }

    public void setMaxFigure2(Coordinate maxFigure2) {
        this.maxFigure2 = maxFigure2;
    }

    public Coordinate getMinFigure1() {
        return minFigure1;
    }

    public void setMinFigure1(Coordinate minFigure1) {
        this.minFigure1 = minFigure1;
    }

    public Coordinate getMinFigure2() {
        return minFigure2;
    }

    public void setMinFigure2(Coordinate minFigure2) {
        this.minFigure2 = minFigure2;
    }

    public int getPlayerOnTurn() {
        return playerOnTurn;
    }

    public void setPlayerOnTurn(int playerTurn) {
        this.playerOnTurn = playerTurn;
    }

    /**
     * Vraca boolean vrednost da li je moguce iz tog polja preci u neko od susednih
     */
    public boolean isBlocked(Coordinate startCoordinate) {
        for (Coordinate destinationCoordinate : startCoordinate.surroundingCoordinates()) {

            if (!isPositionUnreachable(startCoordinate, destinationCoordinate)) {
                return false;
            }
        }
        return true;
    }
}
