package etf.santorini.sl150191d.controller;

import etf.santorini.sl150191d.Main;
import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.log.LogWriter;
import etf.santorini.sl150191d.structures.Coordinate;

/**
 * Klasa kontrolera koriscenog za igru dva igraca.
 */
public class PvPController extends Controller {


    public PvPController(Main main, LogWriter logWriter) {
        super(main, logWriter);
    }

    @Override
    public void reportFieldClicked(Coordinate coordinate) {

        switch (state) {
            case INPUT_MAX_1:

                inputMax1(coordinate);

                gameGUI.setGameInfo(Globals.MSG_MIN_SET_FIGURE);

                //next state
                state = StateType.INPUT_MIN_1;
                break;
            case INPUT_MIN_1:

                if (!Coordinate.sameCoordinate(gameState.getMaxFigure1(), coordinate)) {
                    inputMin1(coordinate);

                    gameGUI.setGameInfo(Globals.MSG_MAX_SET_FIGURE);

                    //next state
                    state = StateType.INPUT_MAX_2;
                }
                break;
            case INPUT_MAX_2:

                if (!Coordinate.sameCoordinate(gameState.getMaxFigure1(), coordinate)
                        && !Coordinate.sameCoordinate(gameState.getMinFigure1(), coordinate)) {
                    inputMax2(coordinate);

                    gameGUI.setGameInfo(Globals.MSG_MIN_SET_FIGURE);

                    //next state
                    state = StateType.INPUT_MIN_2;
                }
                break;
            case INPUT_MIN_2:
                if (!Coordinate.sameCoordinate(gameState.getMaxFigure1(), coordinate)
                        && !Coordinate.sameCoordinate(gameState.getMinFigure1(), coordinate)
                        && !Coordinate.sameCoordinate(gameState.getMaxFigure2(), coordinate)) {

                    inputMin2(coordinate);
                    gameGUI.setGameInfo(Globals.MSG_MAX_SEL_FIG);

                    //next state
                    state = StateType.PLAYER_MAX_TURN_SELECT_FIGURE;
                }
                break;
            case PLAYER_MAX_TURN_SELECT_FIGURE:
                maxSelectFigure(coordinate);
                break;
            case PLAYER_MAX_TURN_SELECT_DESTINATION:
                maxSelectDestination(coordinate);
                break;
            case PLAYER_MAX_TURN_SELECT_BUILD_FIELD:
                maxSelectBuildField(coordinate);
                break;
            case PLAYER_MIN_TURN_SELECT_FIGURE:
                minSelectFigure(coordinate);
                break;
            case PLAYER_MIN_TURN_SELECT_DESTINATION:
                minSelectDestination(coordinate);
                break;
            case PLAYER_MIN_TURN_SELECT_BUILD_FIELD:
                minSelectBuildField(coordinate);
                break;
            case GAME_OVER:

                break;
        }
    }

}
