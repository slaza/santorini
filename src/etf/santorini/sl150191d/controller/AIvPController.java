package etf.santorini.sl150191d.controller;

import etf.santorini.sl150191d.Main;
import etf.santorini.sl150191d.algorithm.estimate.Estimate;
import etf.santorini.sl150191d.algorithm.minimax.Minimax;
import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.log.LogWriter;
import etf.santorini.sl150191d.structures.Coordinate;

public class AIvPController extends AIController {

    public AIvPController(Main main, LogWriter logWriter, Minimax minimax, Estimate estimate, int maxDepth) {
        super(main, logWriter, minimax, estimate, maxDepth);

        inputAIMax1();
        state = StateType.INPUT_MIN_1;
    }

    @Override
    public void reportFieldClicked(Coordinate coordinate) {
        switch (state) {
            case INPUT_MIN_1:
                if (!Coordinate.sameCoordinate(coordinate, gameState.getMaxFigure1())) {
                    inputMin1(coordinate);

                    gameGUI.setGameInfo(Globals.MSG_MAX_SET_FIGURE);

                    //ai turn
                    inputAIMax2();

                    //next state
                    state = StateType.INPUT_MIN_2;
                }
                break;
            case INPUT_MIN_2:

                if (!Coordinate.sameCoordinate(gameState.getMaxFigure1(), coordinate)
                        && !Coordinate.sameCoordinate(gameState.getMaxFigure2(), coordinate)
                        && !Coordinate.sameCoordinate(gameState.getMinFigure1(), coordinate)) {
                    inputMin2(coordinate);

                    gameGUI.setGameInfo(Globals.MSG_MAX_SEL_FIG);

                    //ai turn
                    playWithBot();

                    state = StateType.PLAYER_MIN_TURN_SELECT_FIGURE;
                }
                break;

            case PLAYER_MIN_TURN_SELECT_FIGURE:
                minSelectFigure(coordinate);
                break;
            case PLAYER_MIN_TURN_SELECT_DESTINATION:
                minSelectDestination(coordinate);
                break;
            case PLAYER_MIN_TURN_SELECT_BUILD_FIELD:
                minSelectBuildField(coordinate);

                if (endCoordinate.isNextTo(coordinate) && gameState.isBuildable(coordinate) && !gameState.isMinVictorious()) {
                    /*
                     * Poziva se bot.
                     * */
                    playWithBot();

                    if (gameState.isMaxVictorious()) {
                        gameGUI.setGameInfo(Globals.MSG_MAX_WON);
                        state = StateType.GAME_OVER;
                    } else {
                        gameGUI.setGameInfo(Globals.MSG_MIN_SEL_FIG);
                        state = StateType.PLAYER_MIN_TURN_SELECT_FIGURE;
                    }
                }
                break;
            /*
             * Omogucava nastavak igre iz fajla
             */
            case PLAYER_MAX_TURN_SELECT_FIGURE:
                playWithBot();

                if (gameState.isMaxVictorious()) {
                    gameGUI.setGameInfo(Globals.MSG_MAX_WON);
                    state = StateType.GAME_OVER;
                } else {
                    gameGUI.setGameInfo(Globals.MSG_MIN_SEL_FIG);
                    state = StateType.PLAYER_MIN_TURN_SELECT_FIGURE;
                }
                break;

            default:
                break;
        }
    }

}
