package etf.santorini.sl150191d.controller;

import etf.santorini.sl150191d.Main;
import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.gui.GameGUI;
import etf.santorini.sl150191d.log.LogWriter;
import etf.santorini.sl150191d.structures.Coordinate;
import etf.santorini.sl150191d.structures.GameState;

public abstract class Controller {
    GameGUI gameGUI;
    StateType state;
    LogWriter logWriter;
    protected GameState gameState;
    Coordinate endCoordinate;
    private Main myMain;
    private Coordinate startCoordinate;
    private Coordinate buildCoordinate;

    Controller(Main main, LogWriter logWriter) {
        this.myMain = main;
        this.logWriter = logWriter;

        state = StateType.INPUT_MAX_1;
        gameGUI = new GameGUI(this);
        gameState = new GameState();
        gameState.setPlayerOnTurn(Globals.PLAYER_MAX);
    }

    void inputMax1(Coordinate coordinate) {
        gameState.setMaxFigure1(coordinate);
        gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL0_MAX_IMAGE_PATH);
        gameState.setPlayerOnTurn(Globals.PLAYER_MIN);
    }

    void inputMax2(Coordinate coordinate) {
        gameState.setMaxFigure2(coordinate);
        gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL0_MAX_IMAGE_PATH);
        gameState.setPlayerOnTurn(Globals.PLAYER_MIN);

    }

    void inputMin1(Coordinate coordinate) {
        gameState.setMinFigure1(coordinate);
        gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL0_MIN_IMAGE_PATH);
        gameState.setPlayerOnTurn(Globals.PLAYER_MAX);
    }

    void inputMin2(Coordinate coordinate) {
        gameState.setMinFigure2(coordinate);
        gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL0_MIN_IMAGE_PATH);
        gameState.setPlayerOnTurn(Globals.PLAYER_MAX);

        //vrsi se ispis unetih polja
        logWriter.log(gameState.getMaxFigure1(), gameState.getMaxFigure2());
        logWriter.log(gameState.getMinFigure1(), gameState.getMinFigure2());
    }

    void setFieldEmpty(Coordinate coordinate) {
        switch (gameState.getFieldLevel(coordinate)) {
            case (0):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL0_IMAGE_PATH);
                break;
            case (1):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL1_IMAGE_PATH);
                break;
            case (2):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL2_IMAGE_PATH);
                break;
            case (3):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL3_IMAGE_PATH);
                break;
            case (4):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL4_IMAGE_PATH);
                break;
        }
    }

    void setFieldWithMax(Coordinate coordinate) {
        switch (gameState.getFieldLevel(coordinate)) {
            case (0):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL0_MAX_IMAGE_PATH);
                break;
            case (1):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL1_MAX_IMAGE_PATH);
                break;
            case (2):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL2_MAX_IMAGE_PATH);
                break;
            case (3):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL3_MAX_IMAGE_PATH);
                break;
        }
    }

    void setFieldWithMin(Coordinate coordinate) {
        switch (gameState.getFieldLevel(coordinate)) {
            case (0):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL0_MIN_IMAGE_PATH);
                break;
            case (1):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL1_MIN_IMAGE_PATH);
                break;
            case (2):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL2_MIN_IMAGE_PATH);
                break;
            case (3):
                gameGUI.setFieldImageIcon(coordinate, Globals.LEVEL3_MIN_IMAGE_PATH);
                break;
        }
    }

    public abstract void reportFieldClicked(Coordinate coordinate);

    public void reportNewGame() {
        gameGUI.dispose();
        myMain.newGame();
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public void setState(StateType state) {
        this.state = state;
    }

    public void refreshGUI() {
        for (int row = 0; row < 5; ++row)
            for (int column = 0; column < 5; ++column) {
                setFieldEmpty(new Coordinate(row, column));
            }

        setFieldWithMax(gameState.getMaxFigure1());
        setFieldWithMax(gameState.getMaxFigure2());

        setFieldWithMin(gameState.getMinFigure1());
        setFieldWithMin(gameState.getMinFigure2());

        if (gameState.getPlayerOnTurn() == Globals.PLAYER_MAX)
            if (gameState.isMinVictorious())
                gameGUI.setGameInfo(Globals.MSG_MIN_WON);
            else
                gameGUI.setGameInfo(Globals.MSG_MAX_SEL_FIG);
        else if (gameState.isMaxVictorious())
            gameGUI.setGameInfo(Globals.MSG_MAX_WON);
        else
            gameGUI.setGameInfo(Globals.MSG_MIN_SEL_FIG);
    }

    void maxSelectFigure(Coordinate coordinate) {
        if ((Coordinate.sameCoordinate(gameState.getMaxFigure1(), coordinate)
                || Coordinate.sameCoordinate(gameState.getMaxFigure2(), coordinate))
                && !gameState.isBlocked(coordinate)) {

            if (Coordinate.sameCoordinate(gameState.getMaxFigure1(), coordinate)) {
                startCoordinate = gameState.getMaxFigure1();
            } else {
                startCoordinate = gameState.getMaxFigure2();
            }
            gameGUI.setGameInfo(Globals.MSG_MAX_SEL_DST);

            //next state
            state = StateType.PLAYER_MAX_TURN_SELECT_DESTINATION;
        }
    }

    void maxSelectDestination(Coordinate coordinate) {
        if (!gameState.isPositionUnreachable(startCoordinate, coordinate)) {
            endCoordinate = coordinate;

            setFieldEmpty(startCoordinate);
            setFieldWithMax(endCoordinate);

            if (Coordinate.sameCoordinate(gameState.getMaxFigure1(), startCoordinate)) {
                gameState.setMaxFigure1(endCoordinate);

            } else {
                gameState.setMaxFigure2(endCoordinate);
            }
            gameGUI.setGameInfo(Globals.MSG_MAX_SEL_BUILD);

            //next state
            state = StateType.PLAYER_MAX_TURN_SELECT_BUILD_FIELD;
        } else if (Coordinate.sameCoordinate(gameState.getMaxFigure1(), coordinate)
                || Coordinate.sameCoordinate(gameState.getMaxFigure2(), coordinate))
            startCoordinate = coordinate;
    }

    void maxSelectBuildField(Coordinate coordinate) {
        if (endCoordinate.isNextTo(coordinate) && gameState.isBuildable(coordinate)) {
            gameState.incrementFieldLevel(coordinate);

            buildCoordinate = coordinate;
            setFieldEmpty(buildCoordinate);
            gameState.setPlayerOnTurn(Globals.PLAYER_MIN);

            //next state
            if (gameState.isMaxVictorious()) {
                state = StateType.GAME_OVER;
                gameGUI.setGameInfo(Globals.MSG_MAX_WON);
            } else {
                gameGUI.setGameInfo(Globals.MSG_MIN_SET_FIGURE);
                state = StateType.PLAYER_MIN_TURN_SELECT_FIGURE;
            }

            //log
            logWriter.log(startCoordinate, endCoordinate, buildCoordinate);
        }
    }

    void minSelectFigure(Coordinate coordinate) {
        if ((Coordinate.sameCoordinate(gameState.getMinFigure1(), coordinate)
                || Coordinate.sameCoordinate(gameState.getMinFigure2(), coordinate))
                && !gameState.isBlocked(coordinate)) {

            if (Coordinate.sameCoordinate(gameState.getMinFigure1(), coordinate)) {
                startCoordinate = gameState.getMinFigure1();
            } else {
                startCoordinate = gameState.getMinFigure2();
            }

            gameGUI.setGameInfo(Globals.MSG_MIN_SEL_DST);

            //next state
            state = StateType.PLAYER_MIN_TURN_SELECT_DESTINATION;
        }
    }

    void minSelectDestination(Coordinate coordinate) {
        if (!gameState.isPositionUnreachable(startCoordinate, coordinate)) {
            endCoordinate = coordinate;

            setFieldEmpty(startCoordinate);
            setFieldWithMin(endCoordinate);

            if (Coordinate.sameCoordinate(gameState.getMinFigure1(), startCoordinate)) {
                gameState.setMinFigure1(endCoordinate);

            } else {
                gameState.setMinFigure2(endCoordinate);
            }
            gameGUI.setGameInfo(Globals.MSG_MIN_SEL_BUILD);

            //next state
            state = StateType.PLAYER_MIN_TURN_SELECT_BUILD_FIELD;
        } else if (Coordinate.sameCoordinate(gameState.getMinFigure1(), coordinate)
                || Coordinate.sameCoordinate(gameState.getMinFigure2(), coordinate))
            startCoordinate = coordinate;
    }

    void minSelectBuildField(Coordinate coordinate) {
        if (endCoordinate.isNextTo(coordinate) && gameState.isBuildable(coordinate)) {
            gameState.incrementFieldLevel(coordinate);
            buildCoordinate = coordinate;

            setFieldEmpty(buildCoordinate);
            gameState.setPlayerOnTurn(Globals.PLAYER_MAX);

            //next state
            if (gameState.isMinVictorious()) {
                gameGUI.setGameInfo(Globals.MSG_MIN_WON);
                state = StateType.GAME_OVER;
            } else {
                gameGUI.setGameInfo(Globals.MSG_MAX_SEL_FIG);
                state = StateType.PLAYER_MAX_TURN_SELECT_FIGURE;
            }

            //log
            logWriter.log(startCoordinate, endCoordinate, buildCoordinate);
        }
    }

    public enum StateType {
        INPUT_MAX_1,
        INPUT_MIN_1,

        INPUT_MAX_2,
        INPUT_MIN_2,

        PLAYER_MAX_TURN_SELECT_FIGURE,
        PLAYER_MAX_TURN_SELECT_DESTINATION,
        PLAYER_MAX_TURN_SELECT_BUILD_FIELD,

        PLAYER_MIN_TURN_SELECT_FIGURE,
        PLAYER_MIN_TURN_SELECT_DESTINATION,
        PLAYER_MIN_TURN_SELECT_BUILD_FIELD,

        GAME_OVER
    }
}
