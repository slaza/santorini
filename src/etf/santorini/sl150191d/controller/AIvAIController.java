package etf.santorini.sl150191d.controller;

import etf.santorini.sl150191d.Main;
import etf.santorini.sl150191d.algorithm.estimate.Estimate;
import etf.santorini.sl150191d.algorithm.minimax.Minimax;
import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.log.LogWriter;

/**
 * Kontroler sa dopunjenom playWithBot metodom kako bi se podrzala dva
 * razlicita algoritma igraca.
 */
public abstract class AIvAIController extends AIController {
    private Estimate bot1Est;
    private Estimate bot2Est;
    private Minimax bot1MM;
    private Minimax bot2MM;


    AIvAIController(Main main, LogWriter logWriter, Minimax bot1MM, Estimate bot1Est,
                    Minimax bot2MM, Estimate bot2Est, int maxDepth) {
        super(main, logWriter, bot1MM, bot1Est, maxDepth);

        this.bot1Est = bot1Est;
        this.bot2Est = bot2Est;
        this.bot1MM = bot1MM;
        this.bot2MM = bot2MM;
    }

    @Override
    protected void playWithBot() {
        if (gameState.getPlayerOnTurn() == Globals.PLAYER_MAX) {
            setEstimate(bot1Est);
            setMinimax(bot1MM);
        } else {
            setEstimate(bot2Est);
            setMinimax(bot2MM);
        }

        super.playWithBot();
    }
}
