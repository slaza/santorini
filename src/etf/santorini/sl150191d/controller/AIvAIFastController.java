package etf.santorini.sl150191d.controller;

import etf.santorini.sl150191d.Main;
import etf.santorini.sl150191d.algorithm.estimate.Estimate;
import etf.santorini.sl150191d.algorithm.minimax.Minimax;
import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.log.LogWriter;
import etf.santorini.sl150191d.structures.Coordinate;

public class AIvAIFastController extends AIvAIController implements Runnable {

    public AIvAIFastController(Main main, LogWriter logWriter, Minimax bot1Minimax, Estimate bot1Estimate,
                               Minimax bot2Minimax, Estimate bot2Estimate, int maxDepth) {
        super(main, logWriter, bot1Minimax, bot1Estimate, bot2Minimax, bot2Estimate, maxDepth);
    }

    @Override
    public void reportFieldClicked(Coordinate coordinate) {
        switch (state) {
            case GAME_OVER:
                break;
            case INPUT_MAX_1:
                inputAIMax1();
                inputAIMin1();
                inputAIMax2();
                inputAIMin2();
                /*
                 * Ako se nastavlja partija, preskace se unos pocetnih vrednosti.
                 * */
            default:
                Thread thread = new Thread(this);
                thread.start();

                state = StateType.GAME_OVER;
                break;
        }

    }

    @Override
    public void run() {
        while (true) {
            playWithBot();
            if (gameState.getPlayerOnTurn() == Globals.PLAYER_MIN) {
                gameGUI.setGameInfo(Globals.MSG_MIN_SEL_FIG);
                if (gameState.isMaxVictorious()) {  //poslednji igrao max
                    gameGUI.setGameInfo(Globals.MSG_MAX_WON);
                    state = StateType.GAME_OVER;
                    break;
                }
            } else {
                gameGUI.setGameInfo(Globals.MSG_MAX_SEL_FIG);
                if (gameState.isMinVictorious()) {  //poslednji igrao min
                    gameGUI.setGameInfo(Globals.MSG_MIN_WON);
                    state = StateType.GAME_OVER;
                    break;
                }
            }
        }
    }
}
