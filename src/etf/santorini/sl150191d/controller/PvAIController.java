package etf.santorini.sl150191d.controller;

import etf.santorini.sl150191d.Main;
import etf.santorini.sl150191d.algorithm.estimate.Estimate;
import etf.santorini.sl150191d.algorithm.minimax.Minimax;
import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.log.LogWriter;
import etf.santorini.sl150191d.structures.Coordinate;

public class PvAIController extends AIController {

    public PvAIController(Main main, LogWriter logWriter, Minimax minimax, Estimate estimate, int maxDepth) {
        super(main, logWriter, minimax, estimate, maxDepth);
    }

    @Override
    public void reportFieldClicked(Coordinate coordinate) {
        switch (state) {
            case INPUT_MAX_1:

                inputMax1(coordinate);

                gameGUI.setGameInfo(Globals.MSG_MIN_SET_FIGURE);

                //ai turn
                inputAIMin1();

                //next state
                state = StateType.INPUT_MAX_2;
                break;
            case INPUT_MAX_2:

                if (!Coordinate.sameCoordinate(gameState.getMaxFigure1(), coordinate)
                        && !Coordinate.sameCoordinate(gameState.getMinFigure1(), coordinate)) {
                    inputMax2(coordinate);

                    gameGUI.setGameInfo(Globals.MSG_MIN_SET_FIGURE);

                    //ai turn
                    inputAIMin2();

                    state = StateType.PLAYER_MAX_TURN_SELECT_FIGURE;
                }
                break;

            case PLAYER_MAX_TURN_SELECT_FIGURE:
                maxSelectFigure(coordinate);
                break;
            case PLAYER_MAX_TURN_SELECT_DESTINATION:
                maxSelectDestination(coordinate);
                break;
            case PLAYER_MAX_TURN_SELECT_BUILD_FIELD:
                maxSelectBuildField(coordinate);

                if (endCoordinate.isNextTo(coordinate) && gameState.isBuildable(coordinate) && !gameState.isMaxVictorious()) {
                    /*
                     * Poziva se bot.
                     * */
                    playWithBot();

                    if (gameState.isMinVictorious()) {
                        gameGUI.setGameInfo(Globals.MSG_MIN_WON);
                        state = StateType.GAME_OVER;
                    } else {
                        gameGUI.setGameInfo(Globals.MSG_MAX_SEL_FIG);
                        state = StateType.PLAYER_MAX_TURN_SELECT_FIGURE;
                    }
                }
                break;
            /*
             * Ovaj deo se izvrsava ako je partija ucitana.
             * */
            case PLAYER_MIN_TURN_SELECT_FIGURE:
                playWithBot();

                if (gameState.isMinVictorious()) {
                    gameGUI.setGameInfo(Globals.MSG_MIN_WON);
                    state = StateType.GAME_OVER;
                } else {
                    gameGUI.setGameInfo(Globals.MSG_MAX_SEL_FIG);
                    state = StateType.PLAYER_MAX_TURN_SELECT_FIGURE;
                }
                break;
        }
    }
}
