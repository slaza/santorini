package etf.santorini.sl150191d.controller;

import etf.santorini.sl150191d.Main;
import etf.santorini.sl150191d.algorithm.estimate.Estimate;
import etf.santorini.sl150191d.algorithm.minimax.Minimax;
import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.log.LogWriter;
import etf.santorini.sl150191d.structures.Coordinate;

public class AIvAIStepByStepController extends AIvAIController {

    public AIvAIStepByStepController(Main main, LogWriter logWriter, Minimax bot1Minimax, Estimate bot1Estimate,
                                     Minimax bot2Minimax, Estimate bot2Estimate, int maxDepth) {
        super(main, logWriter, bot1Minimax, bot1Estimate, bot2Minimax, bot2Estimate, maxDepth);
    }

    @Override
    public void reportFieldClicked(Coordinate coordinate) {
        switch (state) {
            case INPUT_MAX_1:
                inputAIMax1();

                state = StateType.INPUT_MIN_1;
                break;
            case INPUT_MIN_1:
                inputAIMin1();

                state = StateType.INPUT_MAX_2;
                break;

            case INPUT_MAX_2:
                inputAIMax2();

                state = StateType.INPUT_MIN_2;
                break;

            case INPUT_MIN_2:
                inputAIMin2();

                state = StateType.PLAYER_MAX_TURN_SELECT_FIGURE;
                break;

            case PLAYER_MAX_TURN_SELECT_FIGURE:
                gameGUI.clearFirstLevelEstimate(getFirstLevelEst());
                playWithBot();
                gameGUI.showFirstLevelEstimate(getFirstLevelEst());
                if (gameState.isMaxVictorious()) {
                    gameGUI.setGameInfo(Globals.MSG_MAX_WON);

                    state = StateType.GAME_OVER;
                } else {
                    state = StateType.PLAYER_MIN_TURN_SELECT_FIGURE;
                    gameGUI.setGameInfo(Globals.MSG_MIN_SEL_FIG);
                    gameGUI.setEstimateInfoMessage(Integer.toString(getMinimaxResult()));
                }
                break;

            case PLAYER_MIN_TURN_SELECT_FIGURE:
                gameGUI.clearFirstLevelEstimate(getFirstLevelEst());
                playWithBot();
                gameGUI.showFirstLevelEstimate(getFirstLevelEst());
                if (gameState.isMinVictorious()) {
                    gameGUI.setGameInfo(Globals.MSG_MIN_WON);

                    state = StateType.GAME_OVER;
                } else {
                    state = StateType.PLAYER_MAX_TURN_SELECT_FIGURE;
                    gameGUI.setGameInfo(Globals.MSG_MAX_SEL_FIG);
                    gameGUI.setEstimateInfoMessage(Integer.toString(getMinimaxResult()));
                }
                break;

            case GAME_OVER:
                break;

            default:
                break;
        }
    }

}
