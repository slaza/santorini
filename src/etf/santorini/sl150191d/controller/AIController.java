package etf.santorini.sl150191d.controller;

import etf.santorini.sl150191d.Main;
import etf.santorini.sl150191d.algorithm.estimate.Estimate;
import etf.santorini.sl150191d.algorithm.minimax.Minimax;
import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.log.LogWriter;
import etf.santorini.sl150191d.structures.Coordinate;
import etf.santorini.sl150191d.structures.GameState;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Random;

/**
 * Dodatne metode za unos figura i igranje poteza od strane kompjutera.
 */
public abstract class AIController extends Controller {
    protected Minimax minimax;
    protected Estimate estimate;
    private int maxDepth;

    private int minimaxResult;
    private ArrayList<Pair<Coordinate, Integer>> firstLevelEst;

    AIController(Main main, LogWriter logWriter, Minimax minimax, Estimate estimate, int maxDepth) {
        super(main, logWriter);
        this.minimax = minimax;
        this.estimate = estimate;
        this.maxDepth = maxDepth;
    }

    protected void playWithBot() {
        minimaxResult = minimax.minimax(null, gameState, estimate, maxDepth, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
        GameState nextGameState = minimax.getBestGameState();

        firstLevelEst = minimax.getFirstLevelEst();
        Coordinate startPosition, endPosition, buildPosition = null;


        /*
         * Postavi izabrani potez.
         * */
        if (gameState.getPlayerOnTurn() == Globals.PLAYER_MAX) {
            /*
             * Max igrac na potezu
             * */
            gameState.setPlayerOnTurn(Globals.PLAYER_MIN);
            if (!Coordinate.sameCoordinate(gameState.getMaxFigure1(), nextGameState.getMaxFigure1())) {
                /*
                 * Pomerena je max figura 1
                 * */
                startPosition = gameState.getMaxFigure1();
                endPosition = nextGameState.getMaxFigure1();

                //obrisi figuru sa stare pozicije, dodaj na novu
                setFieldEmpty(gameState.getMaxFigure1());  //gui
                setFieldWithMax(nextGameState.getMaxFigure1()); //gui
                gameState.setMaxFigure1(nextGameState.getMaxFigure1()); //kontroler

                //dodaj sagradjeni blok
                for (Coordinate coordinate : gameState.buildableAroundCoordinate(gameState.getMaxFigure1())) {
                    if (gameState.getFieldLevel(coordinate) == nextGameState.getFieldLevel(coordinate))
                        continue;

                    //nasli kordinatu
                    gameState.setFieldLevel(coordinate, gameState.getFieldLevel(coordinate) + 1);   //kontroler
                    setFieldEmpty(coordinate);  //gui

                    buildPosition = coordinate;
                    break;
                }

            } else {
                /*
                 * Pomerena je max figura 2
                 * */
                startPosition = gameState.getMaxFigure2();
                endPosition = nextGameState.getMaxFigure2();

                setFieldEmpty(gameState.getMaxFigure2());  //gui
                setFieldWithMax(nextGameState.getMaxFigure2()); //gui
                gameState.setMaxFigure2(nextGameState.getMaxFigure2()); //kontroler

                //dodaj sagradjeni blok
                for (Coordinate coordinate : gameState.buildableAroundCoordinate(gameState.getMaxFigure2())) {
                    if (gameState.getFieldLevel(coordinate) == nextGameState.getFieldLevel(coordinate))
                        continue;

                    //nasli kordinatu
                    gameState.setFieldLevel(coordinate, gameState.getFieldLevel(coordinate) + 1);   //kontroler
                    setFieldEmpty(coordinate);  //gui

                    buildPosition = coordinate;
                    break;
                }
            }
        } else {
            /*
             * Min igrac na potezu
             * */
            gameState.setPlayerOnTurn(Globals.PLAYER_MAX);
            if (!Coordinate.sameCoordinate(gameState.getMinFigure1(), nextGameState.getMinFigure1())) {
                /*
                 * Pomerena je figura 1
                 * */
                startPosition = gameState.getMinFigure1();
                endPosition = nextGameState.getMinFigure1();

                setFieldEmpty(gameState.getMinFigure1());  //gui
                setFieldWithMin(nextGameState.getMinFigure1()); //gui
                gameState.setMinFigure1(nextGameState.getMinFigure1()); //kontroler

                //dodaj sagradjeni blok
                for (Coordinate coordinate : gameState.buildableAroundCoordinate(gameState.getMinFigure1())) {
                    if (gameState.getFieldLevel(coordinate) == nextGameState.getFieldLevel(coordinate))
                        continue;

                    //nasli kordinatu
                    gameState.setFieldLevel(coordinate, gameState.getFieldLevel(coordinate) + 1);   //kontroler
                    setFieldEmpty(coordinate);  //gui

                    buildPosition = coordinate;
                    break;
                }

            } else {
                /*
                 * Pomerena je figura 2
                 * */
                startPosition = gameState.getMinFigure2();
                endPosition = nextGameState.getMinFigure2();

                setFieldEmpty(gameState.getMinFigure2());  //gui
                setFieldWithMin(nextGameState.getMinFigure2()); //gui
                gameState.setMinFigure2(nextGameState.getMinFigure2()); //kontroler

                //dodaj sagradjeni blok
                for (Coordinate coordinate : gameState.buildableAroundCoordinate(gameState.getMinFigure2())) {
                    if (gameState.getFieldLevel(coordinate) == nextGameState.getFieldLevel(coordinate))
                        continue;

                    //nasli kordinatu
                    gameState.setFieldLevel(coordinate, gameState.getFieldLevel(coordinate) + 1);   //kontroler
                    setFieldEmpty(coordinate);  //gui

                    buildPosition = coordinate;
                    break;
                }
            }
        }
        logWriter.log(startPosition, endPosition, buildPosition);
    }

    void inputAIMax1() {
        Random random = new Random();
        int row = random.nextInt(5);
        int column = random.nextInt(5);

        Coordinate max1Coordinate = new Coordinate(row, column);

        gameState.setMaxFigure1(max1Coordinate);
        gameState.setPlayerOnTurn(Globals.PLAYER_MIN);

        gameGUI.setFieldImageIcon(max1Coordinate, Globals.LEVEL0_MAX_IMAGE_PATH);
        gameGUI.setGameInfo(Globals.MSG_MIN_SET_FIGURE);
    }

    void inputAIMin1() {
        Random random = new Random();
        int row = random.nextInt(5);
        int column = random.nextInt(5);

        Coordinate min1Coordinate = new Coordinate(row, column);

        while (Coordinate.sameCoordinate(gameState.getMaxFigure1(), min1Coordinate)) {
            row = random.nextInt(5);
            column = random.nextInt(5);

            min1Coordinate = new Coordinate(row, column);
        }

        gameState.setMinFigure1(min1Coordinate);
        gameState.setPlayerOnTurn(Globals.PLAYER_MAX);

        gameGUI.setFieldImageIcon(min1Coordinate, Globals.LEVEL0_MIN_IMAGE_PATH);
        gameGUI.setGameInfo(Globals.MSG_MAX_SET_FIGURE);
    }

    void inputAIMax2() {
        Random random = new Random();
        int row = random.nextInt(5);
        int column = random.nextInt(5);

        Coordinate max2Coordinate = new Coordinate(row, column);

        while (Coordinate.sameCoordinate(gameState.getMaxFigure1(), max2Coordinate)
                || Coordinate.sameCoordinate(gameState.getMinFigure1(), max2Coordinate)) {
            row = random.nextInt(5);
            column = random.nextInt(5);

            max2Coordinate = new Coordinate(row, column);
        }

        gameState.setMaxFigure2(max2Coordinate);
        gameState.setPlayerOnTurn(Globals.PLAYER_MIN);

        gameGUI.setFieldImageIcon(max2Coordinate, Globals.LEVEL0_MAX_IMAGE_PATH);
        gameGUI.setGameInfo(Globals.MSG_MIN_SET_FIGURE);
    }

    void inputAIMin2() {
        Random random = new Random();
        int row = random.nextInt(5);
        int column = random.nextInt(5);

        Coordinate min2Coordinate = new Coordinate(row, column);

        while (Coordinate.sameCoordinate(gameState.getMaxFigure1(), min2Coordinate)
                || Coordinate.sameCoordinate(gameState.getMaxFigure2(), min2Coordinate)
                || Coordinate.sameCoordinate(gameState.getMinFigure1(), min2Coordinate)) {
            row = random.nextInt(5);
            column = random.nextInt(5);

            min2Coordinate = new Coordinate(row, column);
        }


        gameState.setMinFigure2(min2Coordinate);
        gameState.setPlayerOnTurn(Globals.PLAYER_MAX);

        gameGUI.setFieldImageIcon(min2Coordinate, Globals.LEVEL0_MIN_IMAGE_PATH);
        gameGUI.setGameInfo(Globals.MSG_MAX_SEL_FIG);

        //vrsi se ispis unetih polja
        logWriter.log(gameState.getMaxFigure1(), gameState.getMaxFigure2());
        logWriter.log(gameState.getMinFigure1(), gameState.getMinFigure2());
    }

    public void setMinimax(Minimax minimax) {
        this.minimax = minimax;
    }

    public void setEstimate(Estimate estimate) {
        this.estimate = estimate;
    }

    int getMinimaxResult() {
        return minimaxResult;
    }


    ArrayList<Pair<Coordinate, Integer>> getFirstLevelEst() {
        return firstLevelEst;
    }
}
