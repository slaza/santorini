package etf.santorini.sl150191d;

import etf.santorini.sl150191d.algorithm.estimate.Estimate;
import etf.santorini.sl150191d.algorithm.estimate.StrongEstimate;
import etf.santorini.sl150191d.algorithm.estimate.WeakEstimate;
import etf.santorini.sl150191d.algorithm.minimax.AlphaBetaMinimax;
import etf.santorini.sl150191d.algorithm.minimax.Minimax;
import etf.santorini.sl150191d.algorithm.minimax.RegularMinimax;
import etf.santorini.sl150191d.controller.*;
import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.gui.NewGameGUI;
import etf.santorini.sl150191d.log.LogWriter;
import etf.santorini.sl150191d.structures.GameState;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {

    private NewGameGUI ngGUIInstance;

    public Main() {
        ngGUIInstance = new NewGameGUI(this);
    }

    public static void main(String[] args) {
        new Main();
    }

    public void startGame() {
        int difficulty;
        Estimate bot1Estimate;
        Minimax bot1Minimax;
        Controller myController;

        Minimax bot2Minimax;
        Estimate bot2Estimate;


        //difficulty
        if (ngGUIInstance.getEasyDifficulty().isSelected()) {
            difficulty = Globals.EASY;
        } else if (ngGUIInstance.getMediumDifficulty().isSelected()) {
            difficulty = Globals.MEDIUM;
        } else {
            difficulty = Globals.HARD;
        }

        //estimate
        if (ngGUIInstance.getWeakEst().isSelected()) {
            bot1Estimate = new WeakEstimate();
        } else {
            bot1Estimate = new StrongEstimate();
        }

        //minimax
        if (ngGUIInstance.getAlphaBetaMM().isSelected()) {
            bot1Minimax = new AlphaBetaMinimax();
        } else {
            bot1Minimax = new RegularMinimax();
        }

        //log
        if (ngGUIInstance.getLogPath().getText().equals(ngGUIInstance.getInputFilePath().getText())) {
            JOptionPane.showMessageDialog(ngGUIInstance, "Error! Matching input/output file name.");
            return;
        }

        String logPath = "./temp.txt";
        if (ngGUIInstance.getLogPath().getText().length() != 0)
            logPath = ngGUIInstance.getLogPath().getText();

        LogWriter lw;
        try {
            lw = new LogWriter(logPath);
        } catch (IOException e) {
            System.err.println("Error creating LogWriter to path " + logPath);
            JOptionPane.showMessageDialog(ngGUIInstance, "Error opening log file.");
            return;
        }

        if (ngGUIInstance.getContinueFromFile().isSelected()) {
            //prepisi sadrzaj log-a za citanje u onaj za pisanje.
            try {
                lw.logFromFile(ngGUIInstance.getInputFilePath().getText());
            } catch (IOException e) {
                JOptionPane.showMessageDialog(ngGUIInstance, "Error reading from " + ngGUIInstance.getInputFilePath().getText());
                return;
            }
        }

        //controller
        if (ngGUIInstance.getPvp().isSelected()) {
            myController = new PvPController(this, lw);
        } else if (ngGUIInstance.getPvAI().isSelected()) {
            myController = new PvAIController(this, lw, bot1Minimax, bot1Estimate, difficulty);
        } else if (ngGUIInstance.getAIvP().isSelected()) {
            myController = new AIvPController(this, lw, bot1Minimax, bot1Estimate, difficulty);
        } else {
            if (ngGUIInstance.getBot2AlphaBetaMM().isSelected())
                bot2Minimax = new AlphaBetaMinimax();
            else
                bot2Minimax = new RegularMinimax();

            if (ngGUIInstance.getWeakEst().isSelected())
                bot2Estimate = new WeakEstimate();
            else
                bot2Estimate = new StrongEstimate();

            if (ngGUIInstance.getStepByStep().isSelected()) {
                myController = new AIvAIStepByStepController(this, lw, bot1Minimax, bot1Estimate, bot2Minimax, bot2Estimate, difficulty);
            } else {
                myController = new AIvAIFastController(this, lw, bot1Minimax, bot1Estimate, bot2Minimax, bot2Estimate, difficulty);
            }
        }

        if (ngGUIInstance.getContinueFromFile().isSelected()) {
            //kreiraj novi gameState

            GameState gs;
            try {
                gs = new GameState(ngGUIInstance.getInputFilePath().getText());
            } catch (FileNotFoundException e) {
                JOptionPane.showMessageDialog(ngGUIInstance, "Input file not found!");
                return;
            }

            myController.setGameState(gs);
            if (gs.getPlayerOnTurn() == Globals.PLAYER_MAX) {
                if (gs.isMinVictorious())
                    myController.setState(Controller.StateType.GAME_OVER);
                else
                    myController.setState(Controller.StateType.PLAYER_MAX_TURN_SELECT_FIGURE);
            } else {
                if (gs.isMaxVictorious())
                    myController.setState(Controller.StateType.GAME_OVER);
                else
                    myController.setState(Controller.StateType.PLAYER_MIN_TURN_SELECT_FIGURE);
            }

            myController.refreshGUI();
        }

        ngGUIInstance.dispose();
    }

    public void newGame() {
        ngGUIInstance = new NewGameGUI(this);
    }

}
