package etf.santorini.sl150191d.algorithm.estimate;

import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.structures.Coordinate;
import etf.santorini.sl150191d.structures.GameState;

/**
 * Uzeto u obzir:
 * -Visinu pojedinih figura * LEVEL_MUL.
 * -Broj mogucih polja za igru * MOVES_MUL.
 * -Zarobljena figura je kao mrtva figura. Ne racuna se u proceni.
 * -Nivo na kome se gradi * BUILD_MUL + udaljenost * DIST_MUL
 */
public class StrongEstimate implements Estimate {
    private static final int LEVEL_MUL = 13;
    private static final int BUILD_MUL = 5;
    private static final int MOVES_MUL = 4;
    private static final int DIST_MUL = 3;


    @Override
    public int estimate(GameState prevState, GameState currState) {

        int result = 0;

        switch (currState.getPlayerOnTurn()) {
            case (Globals.PLAYER_MIN):
                /*
                 *  Odigrao je PLAYER_MAX svoj potez, proveri stanje na kraju poteza.
                 */
                if (currState.isMaxVictorious())
                    return Globals.PLAYER_MAX_VICTORY;

                if (!Coordinate.sameCoordinate(prevState.getMaxFigure1(), currState.getMaxFigure1())) {
                    //max1 se pomerio
                    for (Coordinate coordinate : currState.getMaxFigure1().surroundingCoordinates()) {
                        if (prevState.getFieldLevel(coordinate) != currState.getFieldLevel(coordinate)) {
                            result += getBuildEstimate(currState, coordinate);
                            break;
                        }
                    }
                } else {
                    //max2 se pomerio
                    for (Coordinate coordinate : currState.getMaxFigure2().surroundingCoordinates()) {
                        if (prevState.getFieldLevel(coordinate) != currState.getFieldLevel(coordinate)) {
                            result += getBuildEstimate(currState, coordinate);
                            break;
                        }
                    }
                }

                break;
            case (Globals.PLAYER_MAX):
                /*
                 *  Odigrao je PLAYER_MIN svoj potez, proveri stanje na kraju poteza.
                 */
                if (currState.isMinVictorious())
                    return Globals.PLAYER_MIN_VICTORY;

                if (!Coordinate.sameCoordinate(prevState.getMinFigure1(), currState.getMinFigure1())) {
                    //min1 se pomerio
                    for (Coordinate coordinate : currState.getMinFigure1().surroundingCoordinates()) {
                        if (prevState.getFieldLevel(coordinate) != currState.getFieldLevel(coordinate)) {
                            result += getBuildEstimate(currState, coordinate);
                            break;
                        }
                    }
                } else {
                    //max2 se pomerio
                    for (Coordinate coordinate : currState.getMinFigure2().surroundingCoordinates()) {
                        if (prevState.getFieldLevel(coordinate) != currState.getFieldLevel(coordinate)) {
                            result += getBuildEstimate(currState, coordinate);
                            break;
                        }
                    }
                }
                break;
        }

        result += getFigureEstimate(currState, currState.getMaxFigure1());
        result += getFigureEstimate(currState, currState.getMaxFigure2());
        result -= getFigureEstimate(currState, currState.getMinFigure1());
        result -= getFigureEstimate(currState, currState.getMinFigure2());

        return result;
    }

    /**
     * Procena koliko je dobra pozicija na kojoj je figura.
     * visina * LEVEL_MUL + dostupnaPolja * MOVES_MUL
     */
    private int getFigureEstimate(GameState gameState, Coordinate coordinate) {
        int result = 0;

        if (!gameState.isBlocked(coordinate)) {
            result += gameState.getFieldLevel(coordinate) * LEVEL_MUL;

            for (Coordinate nextCoordinate : coordinate.surroundingCoordinates()) {
                if (!gameState.isPositionUnreachable(coordinate, nextCoordinate))
                    result += MOVES_MUL;
            }
        }
        return result;
    }

    /**
     * Vraca procenu mesta na kome se gradi.
     * Vraca negativnu vrednost ako procena odgovara min-u, pozitivnu ako odgovara max-u.
     */
    private int getBuildEstimate(GameState gameState, Coordinate coordinate) {
        return gameState.getFieldLevel(coordinate) * BUILD_MUL + getDistanceEstimate(gameState, coordinate);
    }

    /**
     * Procena koliko je dobra pozicija figure u odnosu na polje na koje se gradi.
     * Treba biti sto blize.
     */
    private int getDistanceEstimate(GameState gameState, Coordinate coordinate) {
        int result = 0;
        if (!gameState.isBlocked(gameState.getMaxFigure1()))
            result -= coordinate.moveDistance(gameState.getMaxFigure1());

        if (!gameState.isBlocked(gameState.getMaxFigure2()))
            result -= coordinate.moveDistance(gameState.getMaxFigure2());

        if (!gameState.isBlocked(gameState.getMinFigure1()))
            result += coordinate.moveDistance(gameState.getMinFigure1());

        if (!gameState.isBlocked(gameState.getMinFigure2()))
            result += coordinate.moveDistance(gameState.getMinFigure2());

        return result * DIST_MUL;
    }
}
