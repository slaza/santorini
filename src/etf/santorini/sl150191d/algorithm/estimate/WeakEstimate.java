package etf.santorini.sl150191d.algorithm.estimate;

import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.structures.Coordinate;
import etf.santorini.sl150191d.structures.GameState;

import java.util.ArrayList;

public class WeakEstimate implements Estimate {

    private int m;
    private int l;

    /**
     * f = m + l
     * m -> nivo odredisnog polja
     * l -> nivo na koji se dodaje plocica * (razlika rastojanja MAX polja i MIN polja)
     */
    @Override
    public int estimate(GameState prevState, GameState currState) {

        switch (currState.getPlayerOnTurn()) {
            case (Globals.PLAYER_MIN):
                /*
                 *  Odigrao je PLAYER_MAX svoj potez, proveri stanje na kraju poteza.
                 */
                if (currState.isMaxVictorious())
                    return Globals.PLAYER_MAX_VICTORY;

                calculateForMaxPlayer(prevState, currState);

                break;
            case (Globals.PLAYER_MAX):
                /*
                 *  Odigrao je PLAYER_MIN svoj potez, proveri stanje na kraju poteza.
                 */
                if (currState.isMinVictorious())
                    return Globals.PLAYER_MIN_VICTORY;

                calculateForMinPlayer(prevState, currState);

                break;
        }

        return m + l;
    }

    private int calculateMDistance(GameState gs, Coordinate coordinate) {
        int distance = 0;
        distance -= coordinate.manhattanDistance(gs.getMaxFigure1());
        distance -= coordinate.manhattanDistance(gs.getMaxFigure2());
        distance += coordinate.manhattanDistance(gs.getMinFigure1());
        distance += coordinate.manhattanDistance(gs.getMinFigure2());

        return distance;
    }

    private void calculateForMaxPlayer(GameState prevState, GameState currState) {
        if (!Coordinate.sameCoordinate(prevState.getMaxFigure1(), currState.getMaxFigure1())) {
            m = currState.getFieldLevel(currState.getMaxFigure1());
            l = 0;

            ArrayList<Coordinate> coordinates = (ArrayList<Coordinate>) currState.getMaxFigure1().surroundingCoordinates();
            for (Coordinate currCoordinate : coordinates) {
                if (prevState.getFieldLevel(currCoordinate) != currState.getFieldLevel(currCoordinate)) {
                    l = currState.getFieldLevel(currCoordinate) * calculateMDistance(currState, currCoordinate);
                    break;
                }
            }
        } else {
            m = currState.getFieldLevel(currState.getMaxFigure2());
            l = 0;

            ArrayList<Coordinate> coordinates = (ArrayList<Coordinate>) currState.getMaxFigure2().surroundingCoordinates();
            for (Coordinate currCoordinate : coordinates)
                if (prevState.getFieldLevel(currCoordinate) != currState.getFieldLevel(currCoordinate)) {
                    l = currState.getFieldLevel(currCoordinate) * calculateMDistance(currState, currCoordinate);
                    break;
                }
        }
    }

    private void calculateForMinPlayer(GameState prevState, GameState currState) {
        if (!Coordinate.sameCoordinate(prevState.getMinFigure1(), currState.getMinFigure1())) {
            m = -currState.getFieldLevel(currState.getMinFigure1());
            l = 0;

            ArrayList<Coordinate> coordinates = (ArrayList<Coordinate>) currState.getMinFigure1().surroundingCoordinates();
            for (Coordinate currCoordinate : coordinates) {
                if (prevState.getFieldLevel(currCoordinate) != currState.getFieldLevel(currCoordinate)) {
                    l = currState.getFieldLevel(currCoordinate) * calculateMDistance(currState, currCoordinate);
                    break;
                }
            }
        } else {
            m = -currState.getFieldLevel(currState.getMinFigure2());
            l = 0;

            ArrayList<Coordinate> coordinates = (ArrayList<Coordinate>) currState.getMinFigure2().surroundingCoordinates();
            for (Coordinate currCoordinate : coordinates)
                if (prevState.getFieldLevel(currCoordinate) != currState.getFieldLevel(currCoordinate)) {
                    l = currState.getFieldLevel(currCoordinate) * calculateMDistance(currState, currCoordinate);
                    break;
                }
        }
    }

}
