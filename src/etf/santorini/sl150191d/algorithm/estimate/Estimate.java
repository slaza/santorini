package etf.santorini.sl150191d.algorithm.estimate;

import etf.santorini.sl150191d.structures.GameState;

public interface Estimate {

    int estimate(GameState prevState, GameState currState);
}
