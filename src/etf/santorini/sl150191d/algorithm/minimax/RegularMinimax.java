package etf.santorini.sl150191d.algorithm.minimax;

import etf.santorini.sl150191d.algorithm.estimate.Estimate;
import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.structures.Coordinate;
import etf.santorini.sl150191d.structures.GameState;
import javafx.util.Pair;

import java.util.ArrayList;

public class RegularMinimax implements Minimax {

    private GameState bestGameState;
    private static int[][] fields = new int[5][5];

    /**
     * Broj argumenata mora da bude neparan. To je na pozivaocu da obezbedi.
     */
    @Override
    public int minimax(GameState prevGameState, GameState currGameState, Estimate estimateFunction, int maxDepth,
                       int currentDepth, int alpha, int beta) {

        /*
         * Pre bilo kakve obrade proveri da li je ovo zavrsno stanje.
         * Ako je kraj igre, ne idi dalje.
         */
        switch (currGameState.getPlayerOnTurn()) {
            case (Globals.PLAYER_MAX):
                if (currGameState.isMinVictorious()) {
                    bestGameState = currGameState;
                    return Globals.PLAYER_MIN_VICTORY;
                }
            case (Globals.PLAYER_MIN):
                if (currGameState.isMaxVictorious()) {
                    bestGameState = currGameState;
                    return Globals.PLAYER_MAX_VICTORY;
                }
                break;
        }

        /*
         * poslednji cvor, vrati sebe.
         */
        if (currentDepth == maxDepth) {
            bestGameState = currGameState;
            return estimateFunction.estimate(prevGameState, currGameState);
        }

        /*
         * Rekurzivna obrada.
         * Pocetna vrednost je najmanja moguca.
         */
        int bestValue = (currGameState.getPlayerOnTurn() == Globals.PLAYER_MAX) ? Integer.MIN_VALUE : Integer.MAX_VALUE;

        if (currentDepth == 0) {
            fields = new int[5][5]; //obrisemo vrednosti polja

            for (int row = 0; row < 5; ++row)
                for (int column = 0; column < 5; ++column)
                    fields[row][column] = bestValue;

        }

        Coordinate currCoordinate = null;
        ArrayList<GameState> possibleNextGameStates = GameState.possibleNextGameStates(currGameState);

        if (currentDepth == 0 && possibleNextGameStates.size() != 0) {
            if (!Coordinate.sameCoordinate(currGameState.getMaxFigure1(), possibleNextGameStates.get(0).getMaxFigure1()))
                currCoordinate = possibleNextGameStates.get(0).getMaxFigure1();

            else if (!Coordinate.sameCoordinate(currGameState.getMaxFigure2(), possibleNextGameStates.get(0).getMaxFigure2()))
                currCoordinate = possibleNextGameStates.get(0).getMaxFigure2();

            else if (!Coordinate.sameCoordinate(currGameState.getMinFigure1(), possibleNextGameStates.get(0).getMinFigure1()))
                currCoordinate = possibleNextGameStates.get(0).getMinFigure1();

            else
                currCoordinate = possibleNextGameStates.get(0).getMinFigure2();
        }

        for (GameState gs : possibleNextGameStates) {
            RegularMinimax rm = new RegularMinimax();

            int currValue = rm.minimax(currGameState, gs, estimateFunction, maxDepth, currentDepth + 1, 0, 0);

            if (currentDepth == 0 && gs.isFieldOccupied(currCoordinate)) {
                if (currGameState.getPlayerOnTurn() == Globals.PLAYER_MAX
                        && fields[currCoordinate.getRow()][currCoordinate.getColumn()] < currValue)
                    fields[currCoordinate.getRow()][currCoordinate.getColumn()] = currValue;
                else if (currGameState.getPlayerOnTurn() == Globals.PLAYER_MIN
                        && fields[currCoordinate.getRow()][currCoordinate.getColumn()] > currValue)
                    fields[currCoordinate.getRow()][currCoordinate.getColumn()] = currValue;
            } else if (currentDepth == 0) { //druga koordinata

                if (!Coordinate.sameCoordinate(currGameState.getMaxFigure1(), gs.getMaxFigure1()))
                    currCoordinate = gs.getMaxFigure1();

                else if (!Coordinate.sameCoordinate(currGameState.getMaxFigure2(), gs.getMaxFigure2()))
                    currCoordinate = gs.getMaxFigure2();

                else if (!Coordinate.sameCoordinate(currGameState.getMinFigure1(), gs.getMinFigure1()))
                    currCoordinate = gs.getMinFigure1();

                else
                    currCoordinate = gs.getMinFigure2();

                //za tu drugu koordinatu izracunaj vrednost
                if (currGameState.getPlayerOnTurn() == Globals.PLAYER_MAX
                        && fields[currCoordinate.getRow()][currCoordinate.getColumn()] < currValue)
                    fields[currCoordinate.getRow()][currCoordinate.getColumn()] = currValue;
                else if (currGameState.getPlayerOnTurn() == Globals.PLAYER_MIN
                        && fields[currCoordinate.getRow()][currCoordinate.getColumn()] > currValue)
                    fields[currCoordinate.getRow()][currCoordinate.getColumn()] = currValue;
            }

            if (currValue > bestValue && currGameState.getPlayerOnTurn() == Globals.PLAYER_MAX) {
                bestGameState = gs;
                bestValue = currValue;
            } else if (currValue < bestValue && currGameState.getPlayerOnTurn() == Globals.PLAYER_MIN) {
                bestGameState = gs;
                bestValue = currValue;
            }
        }

        return bestValue;
    }

    @Override
    public GameState getBestGameState() {
        return bestGameState;
    }

    @Override
    public ArrayList<Pair<Coordinate, Integer>> getFirstLevelEst() {
        ArrayList<Pair<Coordinate, Integer>> result = new ArrayList<>();

        for (int row = 0; row < 5; ++row) {
            for (int column = 0; column < 5; ++column)
                if (fields[row][column] < Integer.MAX_VALUE && fields[row][column] > Integer.MIN_VALUE)
                    result.add(new Pair<>(new Coordinate(row, column), fields[row][column]));
        }
        return result;
    }
}
