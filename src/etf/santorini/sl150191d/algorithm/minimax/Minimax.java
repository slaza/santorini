package etf.santorini.sl150191d.algorithm.minimax;

import etf.santorini.sl150191d.algorithm.estimate.Estimate;
import etf.santorini.sl150191d.structures.Coordinate;
import etf.santorini.sl150191d.structures.GameState;
import javafx.util.Pair;

import java.util.ArrayList;

public interface Minimax {

    int minimax(GameState prevGameState, GameState currGameState, Estimate estimateFunction, int maxDepth, int currentDepth, int alpha, int beta);

    GameState getBestGameState();

    ArrayList<Pair<Coordinate, Integer>> getFirstLevelEst();
}
