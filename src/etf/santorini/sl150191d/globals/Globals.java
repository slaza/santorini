package etf.santorini.sl150191d.globals;

public class Globals {
    public static final int PLAYER_MAX = 1;
    public static final int PLAYER_MIN = -PLAYER_MAX;

    public static final int PLAYER_MAX_VICTORY = 10000;
    public static final int PLAYER_MIN_VICTORY = -PLAYER_MAX_VICTORY;

    public static final String LEVEL0_IMAGE_PATH = "level0.png";
    public static final String LEVEL0_MAX_IMAGE_PATH = "level0max.png";
    public static final String LEVEL0_MIN_IMAGE_PATH = "level0min.png";

    public static final String LEVEL1_IMAGE_PATH = "level1.png";
    public static final String LEVEL1_MAX_IMAGE_PATH = "level1max.png";
    public static final String LEVEL1_MIN_IMAGE_PATH = "level1min.png";

    public static final String LEVEL2_IMAGE_PATH = "level2.png";
    public static final String LEVEL2_MAX_IMAGE_PATH = "level2max.png";
    public static final String LEVEL2_MIN_IMAGE_PATH = "level2min.png";

    public static final String LEVEL3_IMAGE_PATH = "level3.png";
    public static final String LEVEL3_MAX_IMAGE_PATH = "level3max.png";
    public static final String LEVEL3_MIN_IMAGE_PATH = "level3min.png";

    public static final String LEVEL4_IMAGE_PATH = "level4.png";



    public static final String MSG_MAX_SET_FIGURE = "Player 1 set figure";
    public static final String MSG_MIN_SET_FIGURE = "Player 2 set figure";

    public static final String MSG_MAX_SEL_FIG = "Player 1 choose figure";
    public static final String MSG_MAX_SEL_DST = "Player 1 choose destination";
    public static final String MSG_MAX_SEL_BUILD = "Player 1 choose build destination";

    public static final String MSG_MIN_SEL_FIG = "Player 2 choose figure";
    public static final String MSG_MIN_SEL_DST = "Player 2 choose destination";
    public static final String MSG_MIN_SEL_BUILD = "Player 2 choose build destination";

    public static final String MSG_MAX_WON = "Game over. Player 1 won";
    public static final String MSG_MIN_WON = "Game over. Player 2 won";

    public static final int EASY = 2;
    public static final int MEDIUM = 3;
    public static final int HARD = 4;
}
