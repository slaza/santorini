package etf.santorini.sl150191d.gui;

import etf.santorini.sl150191d.controller.Controller;
import etf.santorini.sl150191d.globals.Globals;
import etf.santorini.sl150191d.structures.Coordinate;
import javafx.util.Pair;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class GameGUI extends JFrame {

    private static final int fieldSize = 80;
    private static final int fieldDistance = 10;

    private Controller myController;
    //polja za igru
    private JLabel[][] fields;
    //obavestenja
    private JLabel gameInfo;
    private JLabel estimateInfo;

    public GameGUI(Controller controller) {
        super("Santorini");

        myController = controller;

        gameInfo = new JLabel(Globals.MSG_MAX_SEL_FIG);
        estimateInfo = new JLabel();
        estimateInfo.setHorizontalAlignment(SwingConstants.RIGHT);

        JPanel panel = new JPanel();
        panel.setBounds(fieldSize, fieldSize, fieldSize * 5 + fieldDistance * 4, fieldSize * 5 + fieldDistance * 4);

        panel.setLayout(new BorderLayout());

        JPanel infoPanel = new JPanel(new GridLayout(1, 2));
        infoPanel.add(gameInfo);
        infoPanel.add(estimateInfo);

        panel.add(infoPanel, BorderLayout.PAGE_START);
        panel.add(getFieldPanel(), BorderLayout.CENTER);

        this.add(panel);

        this.setJMenuBar(getMainMenuBar());

        this.setSize(fieldSize * 8, fieldSize * 8);
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setVisible(true);

    }

    public void setFieldImageIcon(Coordinate coordinate, String imagePath) {
        fields[coordinate.getRow()][coordinate.getColumn()].setIcon(new ImageIcon(this.getClass().getResource(imagePath)));
    }

    public void setGameInfo(String value) {
        gameInfo.setText(value);
    }

    private JPanel getFieldPanel() {
        JPanel panel = new JPanel();

        panel.setBackground(Color.gray);
        panel.setBounds(fieldSize, fieldSize, fieldSize * 5 + fieldDistance * 4, fieldSize * 5 + fieldDistance * 4);

        panel.setLayout(new GridLayout(5, 5));

        fields = new JLabel[5][5];
        for (int row = 0; row < 5; ++row)
            for (int column = 0; column < 5; ++column) {
                fields[row][column] = new JLabel();
                fields[row][column].setName(row + "" + column);
                fields[row][column].setEnabled(true);
                fields[row][column].setSize(fieldSize, fieldSize);
                fields[row][column].setVisible(true);
                fields[row][column].setFont(new Font("Serif", Font.BOLD, 14));
                fields[row][column].setHorizontalTextPosition(JLabel.CENTER);
                fields[row][column].setForeground(Color.WHITE);
                fields[row][column].addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        super.mouseClicked(e);
                        JLabel jLabel = (JLabel) e.getSource();
                        int name = Integer.parseInt(jLabel.getName());
                       //TODO: staviti imena labela u formatu A0, B2 -> new Coordinate(jLabel.getName())
                        myController.reportFieldClicked(new Coordinate(name / 10, name % 10));
                    }
                });
                setFieldImageIcon(new Coordinate(row,column), Globals.LEVEL0_IMAGE_PATH);


                panel.add(fields[row][column]);
            }

        return panel;
    }

    private JMenuBar getMainMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        JMenu menu = new JMenu("Options");

        JMenuItem exit = new JMenuItem("Exit");
        exit.addActionListener(e -> {
            this.dispose();
            System.exit(1);
        });

        JMenuItem newGame = new JMenuItem("New game");
        newGame.addActionListener(e -> myController.reportNewGame());

        menu.add(newGame);
        menu.add(exit);

        menuBar.add(menu);

        return menuBar;
    }

    public void setEstimateInfoMessage(String estimateInfo) {
        this.estimateInfo.setText(estimateInfo);
    }

    public void showFirstLevelEstimate(ArrayList<Pair<Coordinate, Integer>> value) {
        if (value == null)
            return;

        for (Pair<Coordinate, Integer> currValue : value) {
            fields[currValue.getKey().getRow()][currValue.getKey().getColumn()].setText(Integer.toString(currValue.getValue()));
        }
    }

    public void clearFirstLevelEstimate(ArrayList<Pair<Coordinate, Integer>> value) {
        if (value == null)
            return;

        for (Pair<Coordinate, Integer> currValue : value) {
            fields[currValue.getKey().getRow()][currValue.getKey().getColumn()].setText("");
        }
    }
}
