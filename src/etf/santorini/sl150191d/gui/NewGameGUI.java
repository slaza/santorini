package etf.santorini.sl150191d.gui;

import etf.santorini.sl150191d.Main;

import javax.swing.*;
import java.awt.*;

public class NewGameGUI extends JFrame {
    private Main myMain;

    //left
    private JRadioButton pvp;
    private JRadioButton pvAI;
    private JRadioButton AIvP;
    //right
    private JRadioButton easyDifficulty;
    private JRadioButton mediumDifficulty;
    private JCheckBox stepByStep;

    private JRadioButton continueFromFile;
    private JTextField inputFilePath;
    private JTextField logPath;

    //center 1
    private JRadioButton weakEst;
    private JRadioButton strongEst;

    private JRadioButton alphaBetaMM;
    private JRadioButton regularMM;
    //center2
    private JRadioButton bot2WeakEst;
    private JRadioButton bot2StrongEst;

    private JRadioButton bot2AlphaBetaMM;
    private JRadioButton bot2RegularMM;

    public NewGameGUI(Main main) {
        super("New Game");

        this.myMain = main;

        this.setLayout(new BorderLayout());
        this.add(createLeftPanel(), BorderLayout.WEST);
        this.add(createRightPanel(), BorderLayout.EAST);
        this.add(createBottomPanel(), BorderLayout.SOUTH);
        this.add(createCentralPanel(), BorderLayout.CENTER);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        disableBot1Options();
        disableBot2Options();

        this.setSize(800, 300);
        this.setResizable(false);
        this.setVisible(true);
    }

    private JPanel createCentralPanel() {
        JPanel jPanel = new JPanel(new GridLayout(1, 2));
        jPanel.add(createBot1CentralPanel());
        jPanel.add(createBot2CentralPanel());

        return jPanel;
    }

    private JPanel createRightPanel() {

        easyDifficulty = new JRadioButton("EASY");
        mediumDifficulty = new JRadioButton("MEDIUM");
        JRadioButton hardDifficulty = new JRadioButton("HARD");

        mediumDifficulty.setSelected(true);

        ButtonGroup selectDifficulty = new ButtonGroup();
        selectDifficulty.add(easyDifficulty);
        selectDifficulty.add(mediumDifficulty);
        selectDifficulty.add(hardDifficulty);

        JPanel difficultyPanel = new JPanel();
        difficultyPanel.setLayout(new GridLayout(4, 1));
        difficultyPanel.add(new JLabel("Difficulty:"));
        difficultyPanel.add(easyDifficulty);
        difficultyPanel.add(mediumDifficulty);
        difficultyPanel.add(hardDifficulty);

        difficultyPanel.setBorder(BorderFactory.createEtchedBorder());

        return difficultyPanel;
    }

    private JPanel createLeftPanel() {
        stepByStep = new JCheckBox("Step by step");
        stepByStep.setEnabled(false);

        pvp = new JRadioButton("Player vs Player");
        pvAI = new JRadioButton("Player vs AI");
        AIvP = new JRadioButton("AI vs Player");
        JRadioButton AIvAI = new JRadioButton("AI vs AI");

        pvp.addActionListener(e -> {
            stepByStep.setEnabled(false);
            disableBot1Options();
            disableBot2Options();
        });
        pvAI.addActionListener(e -> {
            stepByStep.setEnabled(false);
            enableBot1Options();
            disableBot2Options();
        });
        AIvP.addActionListener(e -> {
            stepByStep.setEnabled(false);
            enableBot1Options();
            disableBot2Options();
        });

        AIvAI.addActionListener(e -> {
            stepByStep.setEnabled(true);
            enableBot1Options();
            enableBot2Options();
        });

        pvp.setSelected(true);

        ButtonGroup selectControllerGroup = new ButtonGroup();
        selectControllerGroup.add(pvp);
        selectControllerGroup.add(pvAI);
        selectControllerGroup.add(AIvP);
        selectControllerGroup.add(AIvAI);

        //Mod igre
        JPanel controllerPanel = new JPanel();
        controllerPanel.setLayout(new GridLayout(5, 2));
        controllerPanel.add(new JLabel("Mode:"));
        controllerPanel.add(new Label());
        controllerPanel.add(pvp);
        controllerPanel.add(new Label());
        controllerPanel.add(pvAI);
        controllerPanel.add(new Label());
        controllerPanel.add(AIvP);
        controllerPanel.add(new Label());
        controllerPanel.add(AIvAI);
        controllerPanel.add(stepByStep);

        controllerPanel.setBorder(BorderFactory.createEtchedBorder());

        return controllerPanel;
    }

    private JPanel createBottomPanel() {

        //bottom
        JRadioButton fromBeginning = new JRadioButton("Start from beginning");
        continueFromFile = new JRadioButton("Load from file");

        fromBeginning.setSelected(true);

        ButtonGroup gameStateGroup = new ButtonGroup();
        gameStateGroup.add(continueFromFile);
        gameStateGroup.add(fromBeginning);

        continueFromFile.addActionListener(e -> inputFilePath.setEnabled(true));

        fromBeginning.addActionListener(e -> inputFilePath.setEnabled(false));

        inputFilePath = new JTextField();
        inputFilePath.setEnabled(false);

        JButton startButton = new JButton("Start");
        startButton.addActionListener(e -> myMain.startGame());

        logPath = new JTextField("./temp.txt");

        JPanel panel = new JPanel(new GridLayout(4, 2));
        panel.add(continueFromFile);
        panel.add(fromBeginning);

        panel.add(new Label("Input file: "));
        panel.add(inputFilePath);

        panel.add(new Label("Output file:"));
        panel.add(logPath);

        panel.add(new Label());
        panel.add(startButton);


        panel.setBorder(BorderFactory.createEtchedBorder());
        return panel;
    }

    /**
     * Algoritmi pretrage i funkcije procene za bota 1
     */
    private JPanel createBot1CentralPanel() {

        JPanel estimatePanel = new JPanel(new GridLayout(3, 1));
        weakEst = new JRadioButton("Weak");
        strongEst = new JRadioButton("Strong");

        weakEst.setSelected(true);

        ButtonGroup bg = new ButtonGroup();
        bg.add(weakEst);
        bg.add(strongEst);

        estimatePanel.add(new Label("Estimate:"));
        estimatePanel.add(weakEst);
        estimatePanel.add(strongEst);
        estimatePanel.setBorder(BorderFactory.createEtchedBorder());


        JPanel minimaxPanel = new JPanel(new GridLayout(3, 1));
        alphaBetaMM = new JRadioButton("Alpha-Beta MM");
        regularMM = new JRadioButton("Regular MM");

        alphaBetaMM.setSelected(true);

        bg = new ButtonGroup();
        bg.add(alphaBetaMM);
        bg.add(regularMM);


        minimaxPanel.add(new Label("Minimax:"));
        minimaxPanel.add(regularMM);
        minimaxPanel.add(alphaBetaMM);
        minimaxPanel.setBorder(BorderFactory.createEtchedBorder());

        JPanel panel = new JPanel(new GridLayout(2, 1));
        panel.add(estimatePanel);
        panel.add(minimaxPanel);

        return panel;
    }

    /**
     * Algoritmi pretrage i funkcije procene za bota 2
     */
    private JPanel createBot2CentralPanel() {

        JPanel estimatePanel = new JPanel(new GridLayout(3, 1));
        bot2WeakEst = new JRadioButton("Weak");
        bot2StrongEst = new JRadioButton("Strong");

        bot2WeakEst.setSelected(true);

        ButtonGroup bg = new ButtonGroup();
        bg.add(bot2WeakEst);
        bg.add(bot2StrongEst);

        estimatePanel.add(new Label("Estimate:"));
        estimatePanel.add(bot2WeakEst);
        estimatePanel.add(bot2StrongEst);
        estimatePanel.setBorder(BorderFactory.createEtchedBorder());


        JPanel minimaxPanel = new JPanel(new GridLayout(3, 1));
        bot2AlphaBetaMM = new JRadioButton("Alpha-Beta MM");
        bot2RegularMM = new JRadioButton("Regular MM");

        bot2AlphaBetaMM.setSelected(true);

        bg = new ButtonGroup();
        bg.add(bot2AlphaBetaMM);
        bg.add(bot2RegularMM);


        minimaxPanel.add(new Label("Minimax:"));
        minimaxPanel.add(bot2RegularMM);
        minimaxPanel.add(bot2AlphaBetaMM);
        minimaxPanel.setBorder(BorderFactory.createEtchedBorder());

        JPanel panel = new JPanel(new GridLayout(2, 1));
        panel.add(estimatePanel);
        panel.add(minimaxPanel);

        return panel;
    }

    private void disableBot1Options() {
        weakEst.setEnabled(false);
        strongEst.setEnabled(false);
        alphaBetaMM.setEnabled(false);
        regularMM.setEnabled(false);
    }

    private void disableBot2Options() {
        bot2WeakEst.setEnabled(false);
        bot2StrongEst.setEnabled(false);
        bot2AlphaBetaMM.setEnabled(false);
        bot2RegularMM.setEnabled(false);
    }

    private void enableBot1Options() {
        weakEst.setEnabled(true);
        strongEst.setEnabled(true);
        alphaBetaMM.setEnabled(true);
        regularMM.setEnabled(true);
    }

    private void enableBot2Options() {
        bot2WeakEst.setEnabled(true);
        bot2StrongEst.setEnabled(true);
        bot2AlphaBetaMM.setEnabled(true);
        bot2RegularMM.setEnabled(true);
    }

    public JRadioButton getPvp() {
        return pvp;
    }

    public JRadioButton getPvAI() {
        return pvAI;
    }

    public JRadioButton getAIvP() {
        return AIvP;
    }

    public JRadioButton getEasyDifficulty() {
        return easyDifficulty;
    }

    public JRadioButton getMediumDifficulty() {
        return mediumDifficulty;
    }

    public JCheckBox getStepByStep() {
        return stepByStep;
    }

    public JRadioButton getContinueFromFile() {
        return continueFromFile;
    }

    public JTextField getInputFilePath() {
        return inputFilePath;
    }

    public JTextField getLogPath() {
        return logPath;
    }

    public JRadioButton getWeakEst() {
        return weakEst;
    }

    public JRadioButton getAlphaBetaMM() {
        return alphaBetaMM;
    }

    public JRadioButton getBot2AlphaBetaMM() {
        return bot2AlphaBetaMM;
    }
}
