package etf.santorini.sl150191d.log;

import etf.santorini.sl150191d.structures.Coordinate;

import java.io.*;
import java.util.Scanner;

public class LogWriter {

    private String filePath;

    public LogWriter(String filePath) throws IOException {
        this.filePath = filePath;

        FileWriter fw = new FileWriter(filePath, false);
        fw.close();
    }

    /**
     * Za potrebe cuvanja pocetnih vrednosti.
     */
    public void log(Coordinate c1, Coordinate c2) {
        try (
                FileWriter fw = new FileWriter(filePath, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)
        ) {
            out.println(c1 + " " + c2);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Za potrebe cuvanja jednog poteza.
     */
    public void log(Coordinate begPos, Coordinate endPos, Coordinate buildPos) {
        try (
                FileWriter fw = new FileWriter(filePath, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)
        ) {
            out.println(begPos + " " + endPos + " " + buildPos);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public void logFromFile(String infilePath) throws IOException {
        Scanner sc = new Scanner(new FileInputStream(infilePath));
        FileWriter fw = new FileWriter(filePath, false);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter out = new PrintWriter(bw);

        while (sc.hasNext()) {
            out.println(sc.nextLine());
        }
    }
}
